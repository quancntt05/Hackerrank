package CrackingTheCodingInterview;

public class BruteForce {
	  public static void search(char[] x, char[] y) {
	    int m = x.length;
	    int n = y.length;
	    System.out.print("Các vị trí xuất hiện trong văn bản của xâu mẫu là: ");
	    for (int j = 0; j <= n - m; j++) {
	      for (int i = 0; i < m && x[i] == y[i + j]; i++) {
	        if (i >= m - 1) {
	          System.out.print(j + "    ");
	        }
	      }
	    }
	  }
	  public static void main(String[] args) {
	    search("GCAGAGAG".toCharArray(), "GCATCGCAGAGAGTTATACAGTACG".toCharArray());
	  }
	}
