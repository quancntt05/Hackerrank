package CrackingTheCodingInterview;

import java.lang.reflect.Array;
import java.util.Arrays;

public class FossiSpringCoding {
	public static void main(String[] args) {
		int testcase3 = 99001;
		System.out.println(
				FossiSpringCoding3(testcase3)
				);
	}

	private static String FossiSpringCoding3(int tc) {
		String[] tcArr = Integer.toString(tc).split("");
		String flag;
		for (int i = 0; i < tcArr.length; i++) {
			for (int j = 0; j < tcArr.length-1; j++) {
				if (Integer.parseInt(tcArr[j])>Integer.parseInt(tcArr[j+1])) {
					flag = tcArr[j];
					tcArr[j] = tcArr[j+1];
					tcArr[j+1] = flag;
				}
			}
		}
		return Arrays.toString(tcArr);
	}
}
