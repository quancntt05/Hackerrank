package CrackingTheCodingInterview;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/ctci-ransom-note/problem
 */	
public class RansomNote {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m = in.nextInt();
        int n = in.nextInt();
        String magazine[] = new String[m];
        Map<String, Integer> key = new HashMap<>();
        for(int magazine_i=0; magazine_i < m; magazine_i++){
            magazine[magazine_i] = in.next();
	   		 if (key.containsKey(magazine[magazine_i])) {
					key.replace(magazine[magazine_i], key.get(magazine[magazine_i])+1);
	   		 }else {
	   			 key.put(magazine[magazine_i], 1);
				}
        }
        
        String ransom[] = new String[n];
        int count = 0;
        for(int ransom_i=0; ransom_i < n; ransom_i++){
            ransom[ransom_i] = in.next();
            if (key.containsKey(ransom[ransom_i])) {
				count++;
				key.replace(ransom[ransom_i], key.get(ransom[ransom_i])-1);
	            if (key.get(ransom[ransom_i]) == 0) {
	    			key.remove(ransom[ransom_i]);
	            }
			}

        }
        
        System.out.println((count == n)?"Yes":"No");
    }	
}
