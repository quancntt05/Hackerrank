package CrackingTheCodingInterview;

import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/ctci-bubble-sort/problem
 */

public class BubbleSort {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = in.nextInt();
		}
		bubbleSort(arr);
	}

	private static void bubbleSort(int[] arr) {
		int countSort = 0, flag = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length-1; j++) {
				if (arr[j] > arr[j+1]) {
					flag = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = flag;
					countSort++;
				}
			}
		}
		System.out.println("Array is sorted in " +countSort+ " swaps.");
		System.out.println("First Element: " + arr[0]);
		System.out.println("Last Element: " + arr[arr.length-1]);
	}

}
