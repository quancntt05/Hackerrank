package CrackingTheCodingInterview;

public class AdventureToTubidooDupDupPlanetI {
	public static void main(String[] args) {
		String s = "AABBCC";
		String result = compress(s);
		System.out.println(result);
	}

	private static String compress(String s) {
		int count = 0, j = 0;
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i)==s.charAt(j)) {
				count++;
			}else {
				if (count > 1) {
					builder.append(s.charAt(j));
					builder.append(count);
				}else {
					builder.append(s.charAt(j));
				}
				count = 1;
				j = i;
			}
		}
		if (count > 1) {
			builder.append(s.charAt(j));
			builder.append(count);
		}else {
			builder.append(s.charAt(j));
		}
		return (builder.toString().length()<s.length())?builder.toString():s;
	}

}
