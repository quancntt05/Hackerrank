package CrackingTheCodingInterview;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
Cho mảng số nguyên a[] gồm có n phần tử, trong quá trình kiểm tra thì anh muốn ko cần phải run lại nhưng vẫn thay đổi được n phần tử của mảng a,
mảng a nhập vô các số nguyên ngẫu nhiên như : 5 2 4 8 7 9 3 6 6 6 6, (đánh dấu vị trí đầu tiên là 1, không phải bằng 0, bắt đầu từ số 5 đó) thì anh cần
in ra màn hình:

Vị trí 1 - 2 : Giảm
Vị trí 2 - 4 : Tăng
Vị trí 4 - 5 : Giảm
Vị trí 5 - 6 : Tăng
Vị trí 6 - 7 : Giảm
Vị trí 7 - 8 : Tăng
Vị trí 8 - 11: Bằng
	*/
public class QuizInterviewTMA {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Nhập n (nhập 0 để hủy): ");
		int n = in.nextInt();
		while(n > 0) {
			System.out.print("Nhập số phần tử (khác 0): ");
			n = in.nextInt();
			int[] arr = new int[n+1];
			arr[0] = 0;
			for (int i = 1; i < arr.length; i++) {
				System.out.print("Nhập phần tử thứ "+i+" ");
				arr[i] = in.nextInt();
			}
			System.out.println("Kết quả: ");
			quizInterviewTMA(arr);
		}
		System.out.println("Đã Hủy!!");
	}

	private static void quizInterviewTMA(int[] arr) {
		List<String> s = new ArrayList<String>();
		s.add("0");
		for (int i = 1; i < arr.length-1; i++) {
			if (arr[i] < arr[i+1]) {
				s.add("Tăng");
			}else if(arr[i] > arr[i+1]) {
				s.add("Giảm");
			}else {
				s.add("Bằng");
			}
		}
		int j = 2,k = 1;
		for (int i = 1; i < s.size()-1; i++) {
			if (s.get(i).equals(s.get(j))) {
				j++;
			}else {
				System.out.println("Vị trí "+k+"-"+ j +":"+ s.get(i));
				k = j;
				j=i+2;
			}
		}
		System.out.println("Vị trí "+k+"-"+ j +":"+ s.get(k));
	}
}
