package algorithms;

import java.util.ArrayList;
import java.util.List;

/*
 * https://www.hackerrank.com/challenges/sherlock-and-anagrams
 */
public class SherlockandAnagrams {
	public static void main(String[] args) {
		String s = "ifailuhkqq";
//		int result = sherlockAndAnagrams(s);
		boolean test = checkAnagram("cbxa", "axbc");
		System.out.println(test);
	}

	private static int sherlockAndAnagrams(String s) {
		List<String> key = new ArrayList<>();
		for (int i = 0; i < s.length(); i++) {
			for (int j = i; j < s.length(); j++) {
				key.add(s.substring(i, j + 1));
			}
		}

		int anms = 0;
		for (int i = 0; i < key.size(); i++) {
			for (int j = 0; j < key.size(); j++) {
				if (i != j && checkAnagram(key.get(i), key.get(j))) {
					anms++;
				}
			}
		}
		return anms / 2;
	}

	private static boolean checkAnagram(String a, String b) {
		if (a.length() != b.length())
			return false;

		int[] ar = new int[26];
		for (int i = 0; i < a.length(); i++) {
			ar[a.charAt(i) - 'a']++;
			ar[b.charAt(i) - 'a']--;
		}

		for (int i = 0; i < 26; i++) {
			if (ar[i] != 0) {
				return false;
			}
		}

		return true;
	}
}
