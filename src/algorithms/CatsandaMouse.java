package algorithms;

public class CatsandaMouse {
	public static void main(String[] args) {
		catAndMouse(1, 2, 3);
	}

	private static void catAndMouse(int cat1, int cat2, int mouse) {
		String result = "Mouse C";
		if (mouse-cat1<mouse-cat2) {
			result = "Cat A";
		}else if(mouse-cat1>mouse-cat2) {
			result = "Cat B";
		}
		System.out.println(result);
	}
}
