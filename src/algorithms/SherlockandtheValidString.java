package algorithms;

import java.util.HashMap;
import java.util.Map;

public class SherlockandtheValidString {
	public static void main(String[] args) {
		String s = "hfchdkkbfifgbgebfaahijchgeeeiagkadjfcbekbdaifchkjfejckbiiihegacfbchdihkgbkbddgaefhkdgccjejjaajgijdkd";
		String result = isValid(s);
		System.out.println(result);
	}

	private static String isValid(String s) {
		String result = "NO";
		int count = 0;
		int j = 0;
		Map<Character, Integer> list = new HashMap<>();
		StringBuilder p = new StringBuilder();
		
		for (int i = j; i < s.length(); i++) {
			if (s.charAt(i) == s.charAt(j)) {
				count++;
			}else {
				p.append(count);
				p.append(s.charAt(j));
				list.put(s.charAt(j), count);
				j = i;
				count = 1;
			}
		}
		
		int flag = list.get(s.charAt(0));
		for (char key : list.keySet()) {
			if (!(flag == list.get(key))) {
				return result;
			}
		}
		
		p.append(count);
		p.append(s.charAt(j));
		list.put(s.charAt(j), count);
		
		if (list.get(s.charAt(s.length()-1)) == flag ||
			list.get(s.charAt(s.length()-1))-1 == flag || 
			list.get(s.charAt(s.length()-1))-1 == 0) 
		{
			result = "YES";
		} 
		return result;
	}

}
