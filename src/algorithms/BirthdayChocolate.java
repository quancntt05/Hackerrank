package algorithms;
/*
 * https://www.hackerrank.com/challenges/the-birthday-bar/problem
 */	
public class BirthdayChocolate {
	public static void main(String[] args) {
//		int result = solve(5, new int[] {1,2,1,3,2}, 4, 3);
//		int result = solve(1, new int[] {4}, 4, 1);
//		int result = solve(6, new int[] {1,1,1,1,1,1}, 3, 2);
		int result = solve(55, new int[] {3,5,4,1,2,5,3,4,3,2,1,1,2,4,2,3,4,5,3,1,2,5,4,5,4,1,1,5,3,1,4,5,2,3,2,5,2,5,2,2,1,5,3,2,5,1,2,4,3,1,5,1,3,3,5}, 18, 6);
		System.out.println(result);
	}

	private static int solve(int n, int[] s, int d, int m) {
		int count = 0,kq = 0,j=0;
		for (int i = 0; i <= s.length-m; i++) {
			j = i;
			while(j<i+m){
				kq += s[j];
				j++;
			}
			if (kq == d) {
				count++;
			}
			kq = 0;
	
		}
		return count;
	}
}
