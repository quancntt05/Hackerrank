package algorithms;
/*
 * https://www.hackerrank.com/challenges/breaking-best-and-worst-records/problem
 */
public class BreakingtheRecords {
	public static void main(String[] args) {
		int[] arr = {10,5,20,20,4,5,2,25,1};
		breakingRecords(arr);
	}

	private static void breakingRecords(int[] arr) {
		int highScope = 0, lowScope = 0, flagHigh = arr[0],flagLow = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > flagHigh) {
				flagHigh = arr[i];
				highScope++;
			}else if (arr[i] < flagLow) {
				flagLow = arr[i];
				lowScope++;
			}

		}
		System.out.println(highScope+" "+lowScope);
	}
}
