package algorithms;
/*
 * https://www.hackerrank.com/challenges/palindrome-index/problem
 */
public class PalindromeIndex {
	public static void main(String[] args) {
		String s = "baa",s1 = "aaa", s2 = "abab";
        int result = palindromeIndex(s);
        System.out.println(result);
	}

	private static int palindromeIndex(String s) {
		int idx = -1,i = 0, j = s.length()-1;
			while (i<j) {
				if (s.charAt(i) != s.charAt(j)) {
					if(s.charAt(i+1) == s.charAt(j) && s.charAt(i+2)==s.charAt(j-1)) {
						return i;
					}else {
						return j;
					}
				}
				i++;
				j--;
			}
		return idx;
	}
}
