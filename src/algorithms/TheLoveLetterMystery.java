package algorithms;
/*
 * https://www.hackerrank.com/challenges/the-love-letter-mystery
 */	
public class TheLoveLetterMystery {
	public static void main(String[] args) {
		String s = "abc";
		theLoveLetterMystery(s);
	}

	private static void theLoveLetterMystery(String s) {
		int count = 0;
		for (int i = 0, c=s.length()-1; i < s.length()/2; i++,c--) {
			if (i<c) {
				count += Math.abs(s.charAt(i) - s.charAt(c));
			}
		}
		System.out.println(count);
	}
}
