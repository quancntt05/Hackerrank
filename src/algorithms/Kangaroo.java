package algorithms;
/*
 * https://www.hackerrank.com/challenges/kangaroo/problem
 */	
public class Kangaroo {
	public static void main(String[] args) {
		String result = kangaroo(43,2,70,2);
		System.out.println(result);
	}

	private static String kangaroo(int x1, int v1, int x2, int v2) {
		return ((v1-v2)!= 0)?((x2-x1)%(v1-v2)==0 && (x2-x1)/(v1-v2)>0)?"YES":"NO":"NO";
	}
}
