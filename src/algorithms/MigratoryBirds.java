package algorithms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

/*
 * https://www.hackerrank.com/challenges/migratory-birds/problem
 */
public class MigratoryBirds {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map<Integer, Integer> key = new HashMap<>();
        int[] ar = new int[n];
        for(int ar_i = 0; ar_i < n; ar_i++){
            ar[ar_i] = in.nextInt();
            key.putIfAbsent(ar[ar_i], 0);
            key.put(ar[ar_i], key.get(ar[ar_i])+1);
        }
        int result = 0,temp = 0;
        for (int i : key.keySet()) {
			if(key.get(i)>temp) {
				result = i;
				temp = key.get(i);
			}else if(key.get(i) == temp) {
				result = Math.min(result, i);
				temp = key.get(i);
			}
		}
        System.out.println(result);
    }

}
