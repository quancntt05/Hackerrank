package algorithms;
/*
 * https://www.hackerrank.com/challenges/angry-professor/problem
 */
public class AngryProfessor {
	public static void main(String[] args) {
		int[] a = {-1,-3,2,3};
		String s = angryProfessor(3, a);
		System.out.println(s);
	}

	private static String angryProfessor(int k, int[] a) {
		int count = 0;
		for (int i : a) {
			if (i <= 0) {
				count++;
			}
		}
		return (count >= k) ? "NO" : "YES";
	}
}
