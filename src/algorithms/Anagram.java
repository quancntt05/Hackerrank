package algorithms;
/*
 * https://www.hackerrank.com/challenges/anagram/problem
 */	
public class Anagram {
	public static void main(String[] args) {
		String s = "aaabbb";
		int result = anagram(s);
		System.out.println(result);
	}

	private static int anagram(String s) {
		int count = 0;
		if (s.length()%2 == 1) {
			return -1;
		}else {
			String s1 = s.substring(0, s.length()/2);
			String s2 = s.substring(s.length()/2, s.length());
			for (int i = 0; i < s2.length(); i++) {
				if (s1.contains(String.valueOf(s2.charAt(i)))) {
					s1 = s1.substring(0, s1.indexOf(String.valueOf(s2.charAt(i))))+
					s1.substring(s1.indexOf(String.valueOf(s2.charAt(i)))+1,s1.length());
				}else {
					count++;
				}
			}
		}
		return count;
	}
}
