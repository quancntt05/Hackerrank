package algorithms;
/*
 * https://www.hackerrank.com/challenges/divisible-sum-pairs/problem
 */
public class DivisibleSumPairs {
	public static void main(String[] args) {
		int[] arr = {1,3,2,6,1,2};
		int result = divisibleSumPairs(arr.length,3,arr);
		System.out.println(result);
	}

	private static int divisibleSumPairs(int n, int k, int[] ar) {
		int count = 0;
		for (int i = 0; i < ar.length; i++) {
			for (int j = 0; j < ar.length; j++) {
				if (i<j && (ar[i]+ar[j])%k==0) {
						count++;
				}
			}
		}
		return count;
	}
}
