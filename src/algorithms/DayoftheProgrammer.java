package algorithms;
/*
 * https://www.hackerrank.com/challenges/day-of-the-programmer/problem
 */
public class DayoftheProgrammer {
	public static void main(String[] args) {
		int year = 1800;
        String result = solve(year);
        System.out.println(result);		
	}

	private static String solve(int year) {
		if(year >= 1700 && year <= 1917) {
			if (year%4 == 0) {
				return "12.09."+year;
			}else
				return "13.09."+year;
		}else if((year >= 1919 && year <= 2700)) {
			if (year%400 == 0 || (year%4 == 0 && year%100 != 0)) {
				return "12.09."+year;
			}else
				return "13.09."+year;
		}else {
				return "26.09."+year;
		}
	}
}
