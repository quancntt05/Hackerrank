package algorithms;

import java.util.Arrays;

public class TheHurdleRace {
	public static void main(String[] args) {
		int result = hurdleRace(4,new int[] {1,6,3,5,2});
		System.out.println(result);
	}

	private static int hurdleRace(int k, int[] height) {
		int max = Arrays.stream(height).max().getAsInt();
		return (max-k>0)?max-k:0;
	}
}
