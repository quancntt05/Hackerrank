package algorithms;

import java.util.HashSet;
import java.util.Set;

/*
 * https://www.hackerrank.com/challenges/two-strings/problem
 */

public class TwoString {
	public static void main(String[] args) {
		String s1 = "hello";
		String s2 = "world";
		String result = twoString(s1,s2);
		System.out.println(result);
	}

	private static String twoString(String s1, String s2) {
		Set<Character> map = new HashSet<>();
		for (int i = 0; i < s1.length(); i++) {
			map.add(s1.charAt(i));
		}
		
		for (int i = 0; i < s2.length(); i++) {
			if (map.contains(s2.charAt(i))) {
				return "YES";
			}
		}
		return "NO";
	}
}
