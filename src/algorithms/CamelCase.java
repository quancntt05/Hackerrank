package algorithms;

import java.util.Scanner;

public class CamelCase {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập String:");
		String s = sc.nextLine();
		int num = camelcase(s);
		System.out.println("Camel Case: "+num);
		sc.close();
	}

	private static int camelcase(String s) {
		return s.length() - s.replaceAll("[A-Z]", "").length();
	}
}
