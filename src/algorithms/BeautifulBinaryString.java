package algorithms;

public class BeautifulBinaryString {
	public static void main(String[] args) {
		String s = "0100101010100010110100100110110100011100111110101001011001110111110000101011011111011001111100011101";
		int result = beautifulBinaryString(s);
		System.out.println(result);
	}

	private static int beautifulBinaryString(String s) {
		int count = 0;
		int flag = s.length()%3;
		for (int i = 0; i < s.length()-flag; i++) {
			if(i == s.length()-flag-1) {
				if(s.charAt(i)=='0') {
					break;
				}
			}
			if(s.charAt(i)=='0') {
				if (s.substring(i, i+3).equals("010")) {
					count++;
					i+=2;
				}
			}
		}
		return count;
	}
}
