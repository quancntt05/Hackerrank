package algorithms;



public class Quicksort {
    static void quickSort(int[] arr,int begin, int end) {
    	if (begin < end) {
			int pi = patition(arr,begin, end);
			
			quickSort(arr, begin, pi-1);
			quickSort(arr, pi+1, end);
		}
    }
    
    private static int patition(int[] arr, int begin, int end) {
		// TODO Auto-generated method stub
    	int pivot = arr[end];
    	int flag = -1;
    		
    	int idx = begin-1; //index of smallest element
    	for (int i = begin; i <= end-1; i++) {
			if(arr[i] <= pivot) {
				
				idx++;
				flag = arr[i];
				arr[i] = arr[idx];
				arr[idx] = flag;
			}
		}
		flag = arr[end];
		arr[end] = arr[idx+1];
		arr[idx+1] = flag;
		
		return idx+1;
	}

	private static void printArray(int[] arr) {
		// TODO Auto-generated method stub
    	for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
    	System.out.println();
	}

	public static void main(String[] args) {
    	int[] arr = new int[] {10, 80, 30, 90, 40, 50, 70};
//		int[] arr = new int[] {70, 40, 80, 30, 20, 90};
    	quickSort(arr,0,arr.length-1);
    	printArray(arr);
    }
}
