package algorithms;

import java.util.Scanner;

/*
 * https://www.hackerrank.com/challenges/bon-appetit/problem	
 */
public class BonAppetit {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int n = in.nextInt();
		int k = in.nextInt();
		
		int[] arr = new int[n];
		int bill = 0;
		
		for (int i = 0; i < n; i++) {
			arr[i] = in.nextInt();
			bill += arr[i];
		}
		
		int b_charge = in.nextInt();
		
		if ((bill - arr[k])/2 < b_charge ) {
			System.out.println(b_charge - (bill - arr[k])/2);
		}else
			System.out.println("Bon Appetit");
	}
}
