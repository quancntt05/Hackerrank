package algorithms;

import java.util.HashMap;
import java.util.Map;

/*
 * https://www.hackerrank.com/challenges/picking-numbers/problem
 */
public class PickingNumbers {
	public static void main(String[] args) {
		int[] a = new int[] {4,2,3,4,4,9,98,98,3,3,3,4,2,98,1,98,98,1,1,4,98,2,98,3,9,9,3,1,4,1,98,9,9,2,9,4,2,2,9,98,4,98,1,3,4,9,1,98,98,4,2,3,98,98,1,99,9,98,98,3,98,98,4,98,2,98,4,2,1,1,9,2,4};
//		int[] a = new int[] {4,6,5,3,3,1};
		System.out.println(pickingNumbers(a));
	}

	private static int pickingNumbers(int[] a) {
		int count = 0, count_max = 0;
		Map<Integer, Integer> key = new HashMap<Integer, Integer>();
		for (int i = 0; i < a.length; i++) {
			key.putIfAbsent(a[i], 0);
			key.put(a[i], key.get(a[i])+1);
		}
        for(int i = 0; i < a.length; i++) {
            int foundNumbers = key.get(a[i])+key.getOrDefault(a[i]+1, 0);
            count_max = Math.max(count_max,foundNumbers);
        }
		return count_max;
	}
}
