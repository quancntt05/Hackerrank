package algorithms;
/*
 * https://www.hackerrank.com/challenges/apple-and-orange/problem
 */	
public class AppleandOrange {
	public static void main(String[] args) {
		int[] apple = {-2,1,2};
		int[] orange = {5,-6};
		countApplesAndOranges(7, 11, 5, 15, apple, orange);
	}

	private static void countApplesAndOranges(int s, int t, int a, int b, int[] apple, int[] orange) {
		int scoreApple = 0, scoreOrange = 0;
		for (int i = 0,k = 0; i < apple.length; i++) {
			k = apple[i]+a;
			if(k >=s && k <=t) {
				scoreApple++;
			}
		}
		for (int i = 0,k = 0; i < orange.length; i++) {
			k = orange[i]+b;
			if(k >=s && k <=t) {
				scoreOrange++;
			}
		}
		System.out.println(scoreApple+" "+scoreOrange);
	}
}
