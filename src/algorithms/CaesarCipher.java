package algorithms;


public class CaesarCipher {
	public static void main(String[] args) {
		String s = "middle-Outz";
		caesarcipher(s,2);
	}

	private static void caesarcipher(String s, int k) {
		StringBuilder buider = new StringBuilder(s);
		k = k%26;
		for (int i = 0,charnew = 0; i < s.length(); i++) {
			if (s.charAt(i)>=65 && s.charAt(i)<=90) {
				charnew = s.charAt(i)+k;
				if(charnew>90) {
					charnew = 64+charnew-90;
				}
				buider.setCharAt(i, (char)charnew);
			}else if(s.charAt(i)>=97 && s.charAt(i)<=122){
				charnew = s.charAt(i)+k;
				if(charnew>122) {
					charnew = 96+charnew-122;
				}
				buider.setCharAt(i, (char)charnew);
			}
			
		}
		System.out.println(buider.toString());
	}
}
